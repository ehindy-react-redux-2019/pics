import React from 'react'
import { UnsplashSearchPhotosResult } from '../api/unsplash'
import Styled from 'styled-components'

const StyledSpanDiv = Styled.div<{span: number}>`
  grid-row-end: span ${props => props.span}
`

const StyledImage = Styled.img`
  width: 250px
`

type Props = {
  image: UnsplashSearchPhotosResult
}

class ImageCard extends React.Component<Props> {
  imageRef: React.RefObject<HTMLImageElement> = React.createRef()
  state = { spans: 0 }

  componentDidMount() {
    this.imageRef.current?.addEventListener("load", this.setSpanse)
  }

  setSpanse = () => {
    const height = this.imageRef.current?.clientHeight
    if(!height) return
    const spans = Math.ceil(height / 10)
    this.setState({ spans })
  }

  render() {
    const { urls, description, alt_description } = this.props.image
    return (
      <StyledSpanDiv span={this.state.spans}>
        <StyledImage
          src={urls.regular}
          title={description}
          alt={alt_description}
          ref={this.imageRef}
        />
      </StyledSpanDiv>

    )
  }
}

export default ImageCard