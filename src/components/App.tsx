import React from 'react'
import { unsplash, UnsplashSearchPhotosResult } from '../api/unsplash'
import SearchBar from './SearchBar'
import Styled from 'styled-components'
import ImageList from './ImageList'

const StyledDiv = Styled.div`
  margin-top: 10px
`

class App extends React.Component {
  state = {
    images: []
  }

  onSearchSubmit = async (term: string) => {
    const response = await unsplash.get('/search/photos', {
      params: { query: term }
    });
    const results: UnsplashSearchPhotosResult[] = response.data.results
    this.setState({ images: results })
  }

  render() {
    return (
      <StyledDiv className="ui container">
        <SearchBar onSubmit={this.onSearchSubmit} />
        {this.state.images.length > 0 &&
          <ImageList images={this.state.images} />
        }
      </StyledDiv>
    );
  }
};

export default App
