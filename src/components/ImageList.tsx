import React from 'react'
import { UnsplashSearchPhotosResult } from '../api/unsplash'
import Styled from 'styled-components'
import ImageCard from './ImageCard'

const StyledGridDiv = Styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-gap: 0 10px;
  grid-auto-rows: 10px;
`


type Props = {
  images: UnsplashSearchPhotosResult[]
}

const ImageList = (props: Props) => {
  const images = props.images.map((image) => {
    return(
    <ImageCard
      image={image}
      key={image.id}
      />
    )
  })

  return <StyledGridDiv className="image-list">{images}</StyledGridDiv>
}


export default ImageList